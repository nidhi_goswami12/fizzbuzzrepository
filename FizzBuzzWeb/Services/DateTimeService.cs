﻿namespace FizzBuzzWeb.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    public class DateTimeService : IDateTimeService
    {
        public DateTime GetDateTimeNow()
        {
            return DateTime.Now;
        }
    }
}