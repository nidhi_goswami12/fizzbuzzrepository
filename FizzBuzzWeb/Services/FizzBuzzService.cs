﻿namespace FizzBuzzWeb.Services
{
    using System.Collections.Generic;
    using System.Linq;

    public class FizzBuzzService : IFizzBuzzService
    {
        private readonly IList<IRule> rules;

        public FizzBuzzService(IList<IRule> rules)
        {
            this.rules = rules;
        }

        public IList<string> GetFizzBuzzNumbers(int input)
        {
            var fizzbuzzNumbers = new List<string>();
            for (var number = 1; number <= input; number++)
            {
                var applicationRules = this.rules.FirstOrDefault(x => x.IsMatch(number));
                var fizzbuzznumber = applicationRules != null ? applicationRules.Execute() : number.ToString();
                fizzbuzzNumbers.Add(fizzbuzznumber);
            }

            return fizzbuzzNumbers;
        }
    }
}