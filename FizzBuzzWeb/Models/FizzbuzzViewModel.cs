﻿namespace FizzBuzzWeb.Models
{
    using FizzBuzzWeb.Content;
    using PagedList;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class FizzbuzzViewModel
    {
        [Range(1, 1000, ErrorMessage = "Value should be between 1 to 1000")]
        public int Input { get; set; }
        public IPagedList<string> FizzBuzzNumbers { get; set; }

        public FizzbuzzViewModel()
        {
            this.FizzBuzzNumbers = new PagedList<string>(new List<string>(), 1, FizzBuzzWebConstants.PageSize);
        }
    }
}