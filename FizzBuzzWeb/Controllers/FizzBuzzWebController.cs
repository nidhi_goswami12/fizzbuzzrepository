﻿#pragma warning disable SA1633 // File should have header
namespace FizzBuzzWeb.Controllers
#pragma warning restore SA1633 // File should have header
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using FizzBuzzWeb.Content;
    using FizzBuzzWeb.Models;
    using FizzBuzzWeb.Services;
    using PagedList;

    public class FizzBuzzWebController : Controller
    {
        private readonly IFizzBuzzService service;

        public FizzBuzzWebController(IFizzBuzzService service)
        {
            this.service = service;
        }

        public ActionResult Index(int page = 1, int? input = 0)
        {
            var model = new FizzbuzzViewModel
            {
                FizzBuzzNumbers = new PagedList<string>(new List<string>(), page < 1 ? 1 : page, FizzBuzzWebConstants.PageSize),
                Input = input.HasValue ? input.Value : 0,
            };
            return this.GetFizzBuzzResult(model);
        }

        [HttpPost]
        public ActionResult Index(FizzbuzzViewModel model)
        {
            return this.GetFizzBuzzResult(model);
        }

        private ActionResult GetFizzBuzzResult(FizzbuzzViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                model.FizzBuzzNumbers = this.service.GetFizzBuzzNumbers(input: model.Input).ToPagedList(model.FizzBuzzNumbers.PageNumber, FizzBuzzWebConstants.PageSize);
            }

            return this.View("Index", model);
        }
    }
}