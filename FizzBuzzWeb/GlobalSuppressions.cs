﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "<Pending>", Scope = "type", Target = "~T:FizzBuzzWeb.Services.FizzRule")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "<Pending>", Scope = "member", Target = "~M:FizzBuzzWeb.Services.BuzzRule.Execute~System.String")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "<Pending>", Scope = "member", Target = "~M:FizzBuzzWeb.Services.BuzzRule.IsMatch(System.Int32)~System.Boolean")]
[assembly: SuppressMessage("StyleCop.CSharp.LayoutRules", "SA1516:Elements should be separated by blank line", Justification = "<Pending>", Scope = "member", Target = "~P:FizzBuzzWeb.Models.FizzbuzzViewModel.FizzBuzzNumbers")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1101:Prefix local calls with this", Justification = "not required", Scope = "member", Target = "~M:FizzBuzzWeb.DependencyResolution.DefaultRegistry.#ctor")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "type", Target = "~T:FizzBuzzWeb.Controllers.FizzBuzzWebController")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1208:System using directives should be placed before other using directives", Justification = "not required", Scope = "namespace", Target = "~N:FizzBuzzWeb.Models")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "type", Target = "~T:FizzBuzzWeb.Models.FizzbuzzViewModel")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "not required", Scope = "member", Target = "~M:FizzBuzzWeb.Models.FizzbuzzViewModel.#ctor")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "type", Target = "~T:FizzBuzzWeb.Services.DateTimeService")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "type", Target = "~T:FizzBuzzWeb.RouteConfig")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "type", Target = "~T:FizzBuzzWeb.Content.FizzBuzzWebConstants")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "type", Target = "~T:FizzBuzzWeb.Services.IDateTimeService")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "type", Target = "~T:FizzBuzzWeb.Services.FizzBuzzRule")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "type", Target = "~T:FizzBuzzWeb.Services.BuzzRule")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "type", Target = "~T:FizzBuzzWeb.Services.FizzBuzzService")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "type", Target = "~T:FizzBuzzWeb.Services.IFizzBuzzService")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "type", Target = "~T:FizzBuzzWeb.Services.IRule")]
