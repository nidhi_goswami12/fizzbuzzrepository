﻿namespace FizzBuzzWeb.Content
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using PagedList;

    public class FizzBuzzWebConstants
    {
        public const int PageSize = 20;
    }
}