﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "Not Required", Scope = "type", Target = "~T:FizzBuzzWeb.UnitTests.Controllers.FizzBuzzWebControllerTest")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1413:Use trailing comma in multi-line initializers", Justification = "Not Required", Scope = "member", Target = "~M:FizzBuzzWeb.UnitTests.Controllers.FizzBuzzWebControllerTest.WhetherModelsUpdatedWithExpectedFizzBuzzNumbers")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "Not Required", Scope = "type", Target = "~T:FizzBuzzWeb.UnitTests.Services.DateTimeServiceTest")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "Not Required", Scope = "type", Target = "~T:FizzBuzzWeb.UnitTests.Services.BuzzRuleTest")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "Not Required", Scope = "type", Target = "~T:FizzBuzzWeb.UnitTests.Services.FizzBuzzRuleTest")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "Not Required", Scope = "type", Target = "~T:FizzBuzzWeb.UnitTests.Services.FizzBuzzServiceTest")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1132:Do not combine fields", Justification = "Not Required", Scope = "type", Target = "~T:FizzBuzzWeb.UnitTests.Services.FizzBuzzServiceTest")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "Not Required", Scope = "type", Target = "~T:FizzBuzzWeb.UnitTests.Services.FizzRuleTest")]
