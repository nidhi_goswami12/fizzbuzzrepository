﻿namespace FizzBuzzWeb.UnitTests.Services
{
    using FizzBuzzWeb.Services;
    using FluentAssertions;
    using Xunit;

    public class FizzRuleTest
    {
        [Theory]
        [InlineData(1, false)]
        [InlineData(3, true)]

        public void IsMatchShouldReturnValueBasedOnInput(int input, bool expectated)
        {
            // Arrange
            var rule = new FizzRule();

            // Act
            var isMatch = rule.IsMatch(input);

            // Assert
            isMatch.Should().Be(expectated);

        }

        [Fact]
        public void ExecuteShouldReturnValueBasedOnInput()
        {
            // Arrange
            var rule = new FizzRule();

            // Act
            var execute = rule.Execute();

            // Assert
            execute.Should().Be("fizz");
        }
    }
}

