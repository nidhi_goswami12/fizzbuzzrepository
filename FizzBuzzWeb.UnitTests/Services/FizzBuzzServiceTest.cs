﻿namespace FizzBuzzWeb.UnitTests.Services
{
    using System.Collections.Generic;
    using FizzBuzzWeb.Services;
    using FluentAssertions;
    using Moq;
    using Xunit;

    public class FizzBuzzServiceTest
    {
        private List<IRule> rules;
        private Mock<IRule> ruleFizzBuzz, ruleFizz, ruleBuzz;

        public FizzBuzzServiceTest()
        {
            this.ruleFizzBuzz = new Mock<IRule>();
            this.ruleFizz = new Mock<IRule>();
            this.ruleBuzz. = new Mock<IRule>();
            this.rules = new List<IRule> { this.ruleFizzBuzz.Object, this.ruleFizz.Object, this.ruleBuzz.Object };
        }

        [Fact]
        public void GetFizzBuzzNumber()
        {
            // Arrange
            this.ruleFizzBuzz.Setup(x => x.IsMatch(15)).Returns(true);
            this.ruleFizzBuzz.Setup(x => x.Execute()).Returns("FizzBuzz");
            this.ruleFizz.Setup(x => x.IsMatch(3)).Returns(true);
            this.ruleFizz.Setup(x => x.Execute()).Returns("Fizz");
            this.ruleBuzz.Setup(x => x.IsMatch(5)).Returns(true);
            this.ruleBuzz.Setup(x => x.Execute()).Returns("Buzz");
            var service = new FizzBuzzService(this.rules);

            // Act
            var number = service.GetFizzBuzzNumbers(5);

            // Assert
            number.Should().BeEquivalentTo(new List<string> { "1", "2", "Fizz", "4", "Buzz" });
        }

        [Fact]
        public void GetErrorMessage()
        {
            // Arrange
            var service = new FizzBuzzService(rules);

            // Act
            var numbers = service.GetFizzBuzzNumbers(-1);

            // Assert
            numbers.Should().BeEmpty();
        }
    }
}
