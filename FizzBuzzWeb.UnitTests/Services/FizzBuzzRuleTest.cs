﻿namespace FizzBuzzWeb.UnitTests.Services
{
    using FizzBuzzWeb.Services;
    using FluentAssertions;
    using Xunit;

    public class FizzBuzzRuleTest
    {
        [Theory]
        [InlineData(1, false)]
        [InlineData(15, true)]

        public void IsMatchShouldReturnValueBasedOnInput(int input, bool expectated)
        {
            // Arrange
            var rule = new FizzBuzzRule();

            // Act
            var isMatch = rule.IsMatch(input);

            // Assert
            isMatch.Should().Be(expectated);
        }

        [Fact]
        public void ExecuteShouldReturnValueBasedOnInput()
        {
            // Arrange
            var rule = new FizzBuzzRule();

            // Act
            var execute = rule.Execute();

            // Assert
            execute.Should().Be("fizz buzz");
        }

    }
}
