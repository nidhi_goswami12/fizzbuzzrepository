﻿namespace FizzBuzzWeb.UnitTests.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FizzBuzzWeb.Services;
    using FluentAssertions;
    using Xunit;

    public class DateTimeServiceTest
    {
        [Fact]
        public void WhetherGetDateTimeReturnsCurrentInput()
        {
            // Arrange
            var dateTimeService = new DateTimeService();

            // Act
            var service = dateTimeService.GetDateTimeNow();

            // Assert
            service.ToShortDateString().Should().NotBe(default(DateTime).ToShortDateString());
        }
    }
}
